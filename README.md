Derecho
=======

A lightweight issue tracking system ala Bugzilla, Mantis, Redmine.

Where other similar systems have tons of features and allow you to customize the system to suit your organizational practices, Derecho's aim is to be just large and complex enough to allow you to manage your tasks, and no more.

With Derecho, you can get started immediately, enter new tasks quickly, and avoid complicated configuration screens entirely.

Derecho is written using Symfony, Doctrine ORM, Assetic, Twig, etc.

Features Derecho Has
--------------------

- Project - Milestone - Issue heirarchy
- Issue Status, including archival
- Tags
- Workspans (basic time-tracking)

Features Derecho Will Soon Have
-------------------------------

- Basic Workflows
- Time Tracking
- Full-Text Search
- AJAX Interface
- Integration with VCS, etc.
- Notifications (email only)

Features Derecho Intentionally Omits
------------------------------------

- Users and Login
- Custom Fields
- Complex Workflows (Triggers, etc.)
- CI Integration

Setup
-----

After cloning the repo, you can simply run setup.bash, located in the project's root directory,
to initialize your bug tracker. The script will ask you to fill in a few details, such as your
databse setup; for help with these, refer to the Doctrine and Symfony documentation.

If you do not have a database or just want to try Derecho out, here's a basic configuration:

    database_driver:   pdo_sqlite
    database_host:     127.0.0.1
    database_port:     0
    database_name:     symfony
    database_user:     root
    database_password: ""
    database_path:     /path/to/database/db.sqlite

You should point your webserver to web/app.php. For instance, you can set your server document root to /path/to/derecho/web/, and then open http://your.server/app.php in your browser. For more complex configuration, refer to the Symfony documentation, and your webserver's documentation.

LightTPD is a simple webserver that is easy to configure and works on both Windows and Linux.
As an example, here is the LightTPD vhosts.conf I use to server my personal instance of Derecho.
The server hostname is here represented by "yoursrv", the derecho hostname as "bugs.yoursrv",
the web root directory as "vhosts_dir", and the derecho install path as "vhosts_dir + '/bugs'".

    $SERVER["socket"]==":443"{
        server.document-root=vhosts_dir
        ssl.engine="enable"
        ssl.ca-file=cert_dir+"/chain.pem"
        ssl.pemfile=cert_dir+"/yoursrv.pem"

        $HTTP["host"]=="yoursrv"{
            $HTTP["url"]=~"^/"{
                url.access-deny=("")
            }
        }

        $HTTP["host"]=="bugs.yoursrv"{
            ssl.pemfile=cert_dir+"/bugs.yoursrv.pem"
            server.document-root=vhosts_dir+"/bugs/web"
            accesslog.filename=log_root+"/bugs.log"
            url.access-allow=("")

            #prod
            url.access-deny=("app_dev.php")
            $HTTP["url"]!~"^/(css/|js/)"{
                url.rewrite=("^/(.*)$"=>"/app.php/$1")
            }
            ##dev
            #url.rewrite=("^/(.*)$"=>"/app_dev.php/$1")
        }
    }

    $SERVER["socket"]==":80"{
        server.document-root=vhosts_dir
        accesslog.filename=log_root+"/general.log"

        $HTTP["url"]=~"^/"{
            url.access-deny=("")
        }

        $HTTP["host"]!="yoursrv"{
            $HTTP["host"]=~"([^:/]+)"{
                url.redirect=("^/(.*)$"=>"https://%1:443/$1")
            }
        }
    }

