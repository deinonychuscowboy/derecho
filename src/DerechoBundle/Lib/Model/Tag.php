<?php

namespace DerechoBundle\Lib\Model;

use Doctrine\ORM\Mapping\Column;
use Doctrine\ORM\Mapping\Entity;
use Doctrine\ORM\Mapping\GeneratedValue;
use Doctrine\ORM\Mapping\Id;
use Doctrine\ORM\Mapping\Table;
use Doctrine\ORM\Mapping\ManyToMany;
use Doctrine\ORM\Mapping\JoinTable;
use Doctrine\ORM\Mapping\JoinColumn;
use Doctrine\ORM\PersistentCollection;

/**
 * @Entity
 * @Table(name="Tags")
 */
class Tag extends Model
{
	/**
	 * @var string
	 * @Column(type="string",length=255)
	 */
	private $description;
	/**
	 * @var int
	 * @Id
	 * @GeneratedValue
	 * @Column(type="integer")
	 */
	private $id;
	/**
	 * @var string
	 * @Column(type="string",length=31)
	 */
	private $name;
	/**
	 * @var PersistentCollection
	 * @ManyToMany(targetEntity="Issue",mappedBy="tags")
	 */
	private $issues;
	/**
	 * @var PersistentCollection
	 * @ManyToMany(targetEntity="Milestone",mappedBy="tags")
	 */
	private $milestones;
	/**
	 * @var PersistentCollection
	 * @ManyToMany(targetEntity="Project",mappedBy="tags")
	 */
	private $projects;
	/**
	 * @var PersistentCollection
	 * @ManyToMany(targetEntity="Workspan",mappedBy="tags")
	 */
	private $workspans;

	/**
	 * Create new instance of Tag.
	 *
	 */
	public function __construct()
	{
		$this->issues=[];
		$this->milestones=[];
		$this->projects=[];
		$this->workspans=[];
	}

	/**
	 * @return string
	 */
	public function getDescription()
	{
		return $this->description;
	}

	/**
	 * @param string $description
	 */
	public function setDescription($description)
	{
		$this->description=$description;
	}

	/**
	 * @return int
	 */
	public function getId()
	{
		return $this->id;
	}

	/**
	 * @param int $id
	 */
	public function setId($id)
	{
		$this->id=$id;
	}

	/**
	 * Get the title of this object
	 *
	 * @return string
	 */
	public function getTitle()
	{
		return $this->getName();
	}

	/**
	 * @return string
	 */
	public function getName()
	{
		return $this->name;
	}

	/**
	 * @param string $name
	 */
	public function setName($name)
	{
		$this->name=$name;
	}

	/**
	 * Get value of the Issues.
	 *
	 * @return array
	 */
	public function getIssues()
	{
		return is_array($this->issues)?$this->issues:$this->issues->getValues();
	}

	/**
	 * Get value of the Milestones.
	 *
	 * @return array
	 */
	public function getMilestones()
	{
		return is_array($this->milestones)?$this->milestones:$this->milestones->getValues();
	}

	/**
	 * Get value of the Projects.
	 *
	 * @return array
	 */
	public function getProjects()
	{
		return is_array($this->projects)?$this->projects:$this->projects->getValues();
	}

	/**
	 * Get value of the Workspans.
	 *
	 * @return array
	 */
	public function getWorkspans()
	{
		return is_array($this->workspans)?$this->workspans:$this->workspans->getValues();
	}

	public function getTaggedItems(){
		return array_merge($this->getIssues(),$this->getMilestones(),$this->getProjects(),$this->getWorkspans());
	}

	public function getParentType()
	{
		return null;
	}

	public function getParentId(){
		return null;
	}
}
