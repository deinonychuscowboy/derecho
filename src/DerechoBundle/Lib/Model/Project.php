<?php

namespace DerechoBundle\Lib\Model;

use Doctrine\ORM\Mapping\Column;
use Doctrine\ORM\Mapping\Entity;
use Doctrine\ORM\Mapping\GeneratedValue;
use Doctrine\ORM\Mapping\Id;
use Doctrine\ORM\Mapping\ManyToOne;
use Doctrine\ORM\Mapping\ManyToMany;
use Doctrine\ORM\Mapping\OneToMany;
use Doctrine\ORM\Mapping\Table;
use Doctrine\ORM\Mapping\JoinTable;
use Doctrine\ORM\Mapping\JoinColumn;
use Doctrine\ORM\PersistentCollection;

/**
 * @Entity
 * @Table(name="Projects")
 */
class Project extends Model
{
	/**
	 * @var string
	 * @Column(type="string",length=1023)
	 */
	private $description;
	/**
	 * @var int
	 * @Id
	 * @GeneratedValue
	 * @Column(type="integer")
	 */
	private $id;
	/**
	 * @var PersistentCollection
	 * @OneToMany(targetEntity="Issue",mappedBy="project")
	 */
	private $issues;
	/**
	 * @var PersistentCollection
	 * @OneToMany(targetEntity="Milestone",mappedBy="project")
	 */
	private $milestones;
	/**
	 * @var string
	 * @Column(type="string",length=31,unique=TRUE)
	 */
	private $name;
	/**
	 * @var Status
	 * @ManyToOne(targetEntity="Status",inversedBy="projects")
	 */
	private $status;
	/**
	 * @var PersistentCollection
	 * @ManyToMany(targetEntity="Tag",inversedBy="projects")
	 * @JoinTable(
	 *     name="RelTagsProjects",
	 *     joinColumns={@JoinColumn(name="ProjectID", referencedColumnName="id")},
	 *     inverseJoinColumns={@JoinColumn(name="TagID", referencedColumnName="id")}
	 * )
	 */
	private $tags;

	/**
	 * Create new instance of Project.
	 *
	 */
	public function __construct()
	{
		$this->tags=[];
		$this->issues=[];
		$this->milestones=[];
	}

	/**
	 * @return string
	 */
	public function getDescription()
	{
		return $this->description;
	}

	/**
	 * @param string $description
	 */
	public function setDescription($description)
	{
		$this->description=$description;
	}

	/**
	 * @return int
	 */
	public function getId()
	{
		return $this->id;
	}

	/**
	 * @param int $id
	 */
	public function setId($id)
	{
		$this->id=$id;
	}

	/**
	 * Get the title of this object
	 *
	 * @return string
	 */
	public function getTitle()
	{
		return $this->getName();
	}

	/**
	 * @return array
	 */
	public function getIssues()
	{
		return is_array($this->issues)?$this->issues:$this->issues->getValues();
	}

	/**
	 * @param array $issues
	 */
	public function setIssues($issues)
	{
		$this->issues=$issues;
	}

	/**
	 * @return array
	 */
	public function getMilestones()
	{
		return is_array($this->milestones)?$this->milestones:$this->milestones->getValues();
	}

	/**
	 * @param array $milestones
	 */
	public function setMilestones($milestones)
	{
		$this->milestones=$milestones;
	}

	/**
	 * @return string
	 */
	public function getName()
	{
		return $this->name;
	}

	/**
	 * @param string $name
	 */
	public function setName($name)
	{
		$this->name=$name;
	}

	/**
	 * @return Status
	 */
	public function getStatus()
	{
		return $this->status;
	}

	/**
	 * @param Status $status
	 */
	public function setStatus($status)
	{
		$this->status=$status;
	}

	/**
	 * Get value of the Tags.
	 *
	 * @return array
	 */
	public function getTags()
	{
		return is_array($this->tags)?$this->tags:$this->tags->getValues();
	}

	/**
	 * Set value of the Tags.
	 *
	 * @param array $tags
	 */
	public function setTags($tags)
	{
		$this->tags=$tags;
	}

	public function setTag($tag)
	{
		if(!in_array($tag,$this->tags))
		{
			$this->tags[]=$tag;
		}
	}

	public function getParentType()
	{
		return null;
	}

	public function getParentId(){
		return null;
	}
}
