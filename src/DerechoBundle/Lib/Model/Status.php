<?php

namespace DerechoBundle\Lib\Model;

use Doctrine\ORM\Mapping\Column;
use Doctrine\ORM\Mapping\Entity;
use Doctrine\ORM\Mapping\GeneratedValue;
use Doctrine\ORM\Mapping\Id;
use Doctrine\ORM\Mapping\OneToMany;
use Doctrine\ORM\Mapping\Table;
use Doctrine\ORM\PersistentCollection;

/**
 * @Entity
 * @Table(name="Statuses")
 */
class Status extends Model
{
	/**
	 * @var string
	 * @Column(type="string",length=255)
	 */
	private $description;
	/**
	 * @var int
	 * @Id
	 * @GeneratedValue
	 * @Column(type="integer")
	 */
	private $id;
	/**
	 * @var string
	 * @Column(type="string",length=31)
	 */
	private $name;
	/**
	 * @var bool
	 * @Column(type="boolean")
	 */
	private $archival;
	/**
	 * @var PersistentCollection
	 * @OneToMany(targetEntity="Issue",mappedBy="status")
	 */
	private $issues;
	/**
	 * @var PersistentCollection
	 * @OneToMany(targetEntity="Milestone",mappedBy="status")
	 */
	private $milestones;
	/**
	 * @var PersistentCollection
	 * @OneToMany(targetEntity="Project",mappedBy="status")
	 */
	private $projects;

	/**
	 * Create new instance of Status.
	 *
	 */
	public function __construct()
	{
		$this->issues    =[];
		$this->milestones=[];
		$this->projects  =[];
	}

	/**
	 * @return string
	 */
	public function getDescription()
	{
		return $this->description;
	}

	/**
	 * @param string $description
	 */
	public function setDescription($description)
	{
		$this->description=$description;
	}

	/**
	 * @return int
	 */
	public function getId()
	{
		return $this->id;
	}

	/**
	 * @param int $id
	 */
	public function setId($id)
	{
		$this->id=$id;
	}

	/**
	 * Get the title of this object
	 *
	 * @return string
	 */
	public function getTitle()
	{
		return $this->getName();
	}

	/**
	 * @return string
	 */
	public function getName()
	{
		return $this->name;
	}

	/**
	 * @param string $name
	 */
	public function setName($name)
	{
		$this->name=$name;
	}

	/**
	 * Get value of the Issues.
	 *
	 * @return array
	 */
	public function getIssues()
	{
		return is_array($this->issues)?$this->issues:$this->issues->getValues();
	}

	/**
	 * Get value of the Milestones.
	 *
	 * @return array
	 */
	public function getMilestones()
	{
		return is_array($this->milestones)?$this->milestones:$this->milestones->getValues();
	}

	/**
	 * Get value of the Projects.
	 *
	 * @return array
	 */
	public function getProjects()
	{
		return is_array($this->projects)?$this->projects:$this->projects->getValues();
	}

	/**
	 * Get value of the Archival.
	 *
	 * @return boolean
	 */
	public function isArchival()
	{
		return $this->archival;
	}

	/**
	 * Set value of the Archival.
	 *
	 * @param boolean $archival
	 */
	public function setArchival($archival)
	{
		$this->archival=$archival;
	}

	public function getParentType()
	{
		return null;
	}

	public function getParentId(){
		return null;
	}
}
