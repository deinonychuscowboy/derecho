<?php

namespace DerechoBundle\Lib\Model;

class Version
{
	/**
	 * @var int
	 */
	private $major;
	/**
	 * @var int
	 */
	private $minor;
	/**
	 * @var int
	 */
	private $revision;

	protected function __construct()
	{
	}

	/**
	 * Version constructor.
	 *
	 * @param int $major
	 * @param int $minor
	 * @param int $revision
	 *
	 * @return Version
	 */
	public static function create($major,$minor,$revision)
	{
		$instance          =new Version();
		$instance->major   =$major;
		$instance->minor   =$minor;
		$instance->revision=$revision;

		return $instance;
	}

	/**
	 * Version constructor.
	 *
	 * @param string $version
	 *
	 * @return Version
	 */
	public static function createFromVersionString($version)
	{
		list($major,$minor,$revision)=explode(".",$version);

		return self::create($major,$minor,$revision);
	}

	function __toString()
	{
		return $this->major.".".$this->minor.".".$this->revision;
	}

	/**
	 * @return int
	 */
	public function getMajor()
	{
		return $this->major;
	}

	/**
	 * @return int
	 */
	public function getMinor()
	{
		return $this->minor;
	}

	/**
	 * @return int
	 */
	public function getRevision()
	{
		return $this->revision;
	}
}
