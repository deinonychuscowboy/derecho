<?php

namespace DerechoBundle\Lib\Model;

use Doctrine\ORM\Mapping\Column;
use Doctrine\ORM\Mapping\Entity;
use Doctrine\ORM\Mapping\Id;
use Doctrine\ORM\Mapping\Table;

/**
 * @Entity
 * @Table(name="Settings")
 */
class Setting extends Model
{
	/**
	 * @var string
	 * @Id
	 * @Column(type="string",length=64)
	 */
	private $id;
	/**
	 * @var string
	 * @Column(type="string",length=128)
	 */
	private $value;

	/**
	 * Create new instance of Setting.
	 *
	 * @param string $id
	 * @param string $value
	 */
	public function __construct($id,$value)
	{
		$this->id   =$id;
		$this->value=$value;
	}

	/**
	 * Get value of the Id.
	 *
	 * @return string
	 */
	public function getId()
	{
		return $this->id;
	}

	/**
	 * Get value of the Value.
	 *
	 * @return string
	 */
	public function getValue()
	{
		return $this->value;
	}

	/**
	 * Set value of the Value.
	 *
	 * @param string $value
	 */
	public function setValue($value)
	{
		$this->value=$value;
	}

	/**
	 * Get the title of this object
	 *
	 * @return string
	 */
	public function getTitle()
	{
		return $this->id;
	}

	public function getParentType()
	{
		return null;
	}

	public function getParentId(){
		return null;
	}
}
