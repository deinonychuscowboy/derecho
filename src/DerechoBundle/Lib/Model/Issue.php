<?php

namespace DerechoBundle\Lib\Model;

use Doctrine\ORM\Mapping\Column;
use Doctrine\ORM\Mapping\Entity;
use Doctrine\ORM\Mapping\GeneratedValue;
use Doctrine\ORM\Mapping\Id;
use Doctrine\ORM\Mapping\ManyToOne;
use Doctrine\ORM\Mapping\ManyToMany;
use Doctrine\ORM\Mapping\OneToMany;
use Doctrine\ORM\Mapping\Table;
use Doctrine\ORM\Mapping\JoinTable;
use Doctrine\ORM\Mapping\JoinColumn;
use Doctrine\ORM\PersistentCollection;

/**
 * @Entity
 * @Table(name="Issues")
 */
class Issue extends Model
{
	/**
	 * @var int
	 * @Column(type="integer",nullable=true)
	 */
	private $closedTimestamp;
	/**
	 * @var string
	 * @Column(type="string",length=1024)
	 */
	private $description;
	/**
	 * @var int
	 * @Id
	 * @GeneratedValue
	 * @Column(type="integer")
	 */
	private $id;
	/**
	 * @var Milestone
	 * @ManyToOne(targetEntity="Milestone",inversedBy="issues")
	 */
	private $milestone;
	/**
	 * @var int
	 * @Column(type="integer")
	 */
	private $openedTimestamp;
	/**
	 * @var Project
	 * @ManyToOne(targetEntity="Project",inversedBy="issues")
	 */
	private $project;
	/**
	 * @var Status
	 * @ManyToOne(targetEntity="Status",inversedBy="issues")
	 */
	private $status;
	/**
	 * @var string
	 * @Column(type="string",length=255)
	 */
	private $summary;
	/**
	 * @var PersistentCollection
	 * @OneToMany(targetEntity="Workspan",mappedBy="issue")
	 */
	private $workspans;
	/**
	 * @var PersistentCollection
	 * @ManyToMany(targetEntity="Tag",inversedBy="issues")
	 * @JoinTable(
	 *     name="RelTagsIssues",
	 *     joinColumns={@JoinColumn(name="IssueID", referencedColumnName="id")},
	 *     inverseJoinColumns={@JoinColumn(name="TagID", referencedColumnName="id")}
	 * )
	 */
	private $tags;

	/**
	 * Create new instance of Issue.
	 *
	 */
	public function __construct()
	{
		$this->workspans=[];
		$this->tags=[];
		$this->openedTimestamp=time();
	}

	/**
	 * @return int
	 */
	public function getClosedTimestamp()
	{
		return $this->closedTimestamp;
	}

	/**
	 * @param int $closedTimestamp
	 */
	public function setClosedTimestamp($closedTimestamp)
	{
		$this->closedTimestamp=$closedTimestamp===null||is_int($closedTimestamp)
			?$closedTimestamp
			:$closedTimestamp->getTimestamp();
	}

	/**
	 * @return string
	 */
	public function getDescription()
	{
		return $this->description;
	}

	/**
	 * @param string $description
	 */
	public function setDescription($description)
	{
		$this->description=$description;
	}

	/**
	 * @return int
	 */
	public function getId()
	{
		return $this->id;
	}

	/**
	 * @param int $id
	 */
	public function setId($id)
	{
		$this->id=$id;
	}

	/**
	 * Get the title of this object
	 *
	 * @return string
	 */
	public function getTitle()
	{
		return $this->getSummary();
	}

	/**
	 * @return mixed
	 */
	public function getMilestone()
	{
		return $this->milestone;
	}

	/**
	 * @param mixed $milestone
	 */
	public function setMilestone($milestone)
	{
		$this->milestone=$milestone;
	}

	/**
	 * @return int
	 */
	public function getOpenedTimestamp()
	{
		return $this->openedTimestamp;
	}

	/**
	 * @param int $openedTimestamp
	 */
	public function setOpenedTimestamp($openedTimestamp)
	{

		$this->openedTimestamp=$openedTimestamp===null||is_int($openedTimestamp)
			?$openedTimestamp
			:$openedTimestamp->getTimestamp();
	}

	/**
	 * @return Project
	 */
	public function getProject()
	{
		return $this->project;
	}

	/**
	 * @param Project $project
	 */
	public function setProject($project)
	{
		$this->project=$project;
	}

	/**
	 * @return Status
	 */
	public function getStatus()
	{
		return $this->status;
	}

	/**
	 * @param Status $status
	 */
	public function setStatus($status)
	{
		$this->status=$status;
	}

	/**
	 * @return string
	 */
	public function getSummary()
	{
		return $this->summary;
	}

	/**
	 * @param string $summary
	 */
	public function setSummary($summary)
	{
		$this->summary=$summary;
	}

	/**
	 * @return array
	 */
	public function getWorkspans()
	{
		return is_array($this->workspans)?$this->workspans:$this->workspans->getValues();
	}

	/**
	 * @param array $workspans
	 */
	public function setWorkspans($workspans)
	{
		$this->workspans=$workspans;
	}

	/**
	 * Get value of the Tags.
	 *
	 * @return array
	 */
	public function getTags()
	{
		return is_array($this->tags)?$this->tags:$this->tags->getValues();
	}

	/**
	 * Set value of the Tags.
	 *
	 * @param array $tags
	 */
	public function setTags($tags)
	{
		$this->tags=$tags;
	}

	public function setTag($tag)
	{
		if(!in_array($tag,$this->tags))
		{
			$this->tags[]=$tag;
		}
	}

	public function getParentType()
	{
		return "Project";
	}

	public function getParentId(){
		return $this->getProject()!=null?$this->getProject()->getId():null;
	}
}
