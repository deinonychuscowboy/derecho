<?php

namespace DerechoBundle\Lib\Model;

use Doctrine\ORM\Mapping\Column;
use Doctrine\ORM\Mapping\Entity;
use Doctrine\ORM\Mapping\GeneratedValue;
use Doctrine\ORM\Mapping\Id;
use Doctrine\ORM\Mapping\ManyToOne;
use Doctrine\ORM\Mapping\ManyToMany;
use Doctrine\ORM\Mapping\OneToOne;
use Doctrine\ORM\Mapping\Table;
use Doctrine\ORM\Mapping\JoinTable;
use Doctrine\ORM\Mapping\JoinColumn;
use Doctrine\ORM\PersistentCollection;

/**
 * @Entity
 * @Table(name="Workspans")
 */
class Workspan extends Model
{
	/**
	 * @var string
	 * @Column(type="string",length=1024)
	 */
	private $description;
	/**
	 * @var int
	 * @Column(type="integer",nullable=true)
	 */
	private $endTimestamp;
	/**
	 * @var int
	 * @Id
	 * @GeneratedValue
	 * @Column(type="integer")
	 */
	private $id;
	/**
	 * @var Issue
	 * @ManyToOne(targetEntity="Issue",inversedBy="workspans")
	 */
	private $issue;
	/**
	 * @var int
	 * @Column(type="integer")
	 */
	private $startTimestamp;
	/**
	 * @var PersistentCollection
	 * @ManyToMany(targetEntity="Tag",inversedBy="workspans")
	 * @JoinTable(
	 *     name="RelTagsWorkspans",
	 *     joinColumns={@JoinColumn(name="WorkspanID", referencedColumnName="id")},
	 *     inverseJoinColumns={@JoinColumn(name="TagID", referencedColumnName="id")}
	 * )
	 */
	private $tags;

	/**
	 * Create new instance of Workspan.
	 *
	 */
	public function __construct()
	{
		$this->tags=[];
	}

	/**
	 * @return string
	 */
	public function getDescription()
	{
		return $this->description;
	}

	/**
	 * @param string $description
	 */
	public function setDescription($description)
	{
		$this->description=$description;
	}

	/**
	 * @return int
	 */
	public function getEndTimestamp()
	{
		return $this->endTimestamp;
	}

	/**
	 * @param int $endTimestamp
	 */
	public function setEndTimestamp($endTimestamp)
	{
		$this->endTimestamp=$endTimestamp;
	}

	/**
	 * @return int
	 */
	public function getId()
	{
		return $this->id;
	}

	/**
	 * @param int $id
	 */
	public function setId($id)
	{
		$this->id=$id;
	}

	/**
	 * Get the title of this object
	 *
	 * @return string
	 */
	public function getTitle()
	{
		// TODO respect date formatting
		return $this->getStartTimestamp()." - ".$this->getEndTimestamp();
	}

	/**
	 * @return Issue
	 */
	public function getIssue()
	{
		return $this->issue;
	}

	/**
	 * @param Issue $issue
	 */
	public function setIssue($issue)
	{
		$this->issue=$issue;
	}

	/**
	 * @return int
	 */
	public function getStartTimestamp()
	{
		return $this->startTimestamp;
	}

	/**
	 * @param int $startTimestamp
	 */
	public function setStartTimestamp($startTimestamp)
	{
		$this->startTimestamp=$startTimestamp;
	}

	/**
	 * Get value of the Tags.
	 *
	 * @return array
	 */
	public function getTags()
	{
		return is_array($this->tags)?$this->tags:$this->tags->getValues();
	}

	/**
	 * Set value of the Tags.
	 *
	 * @param array $tags
	 */
	public function setTags($tags)
	{
		$this->tags=$tags;
	}

	public function setTag($tag)
	{
		if(!in_array($tag,$this->tags))
		{
			$this->tags[]=$tag;
		}
	}

	public function getParentType()
	{
		return "Issue";
	}

	public function getParentId(){
		return $this->getIssue()!=null?$this->getIssue()->getId():null;
	}
}
