<?php

namespace DerechoBundle\Lib\Model;

use DerechoBundle\Lib\ContainerAccess;

abstract class Model
{
	public static function filterActive($objects)
	{
		// TODO this doesn't belong here.
		return array_filter(
			$objects,
			function($object)
			{
				return !method_exists($object,"getStatus")||$object->getStatus()===null||!$object->getStatus()
					->isArchival();
			}
		);
	}

	public static function load($id=null)
	{
		$objects=[];
		if($id===null)
		{
			$objects=ContainerAccess::get("doctrine")->getRepository(static::getClass())->findAll();
		}
		elseif(is_array($id))
		{
			$objects=ContainerAccess::get("doctrine")->getRepository(static::getClass())->findBy($id);
		}
		else
		{
			$objects=ContainerAccess::get("doctrine")->getRepository(static::getClass())->find($id);
		}

		return $objects;
	}

	public static function loadActive($id=null)
	{
		// TODO in a sane world, this would be done in the query.
		$objects=static::load($id);

		return self::filterActive($objects);
	}

	public static function name($name)
	{
		return ucfirst(strtolower($name));
	}

	/**
	 * Get the type-unique ID of this object
	 *
	 * @return int
	 */
	public abstract function getId();

	/**
	 * Get the title of this object
	 *
	 * @return string
	 */
	public abstract function getTitle();

	public function __toString()
	{
		return $this->getTitle();
	}

	/**
	 * Get the class name.
	 *
	 * TODO Can be replaced by ::class when we drop 5.4 support.
	 * @return string
	 */
	public static function getClass(){
		return get_called_class();
	}

	public abstract function getParentType();
	public abstract function getParentId();
}
