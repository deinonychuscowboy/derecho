<?php

namespace DerechoBundle\Lib\Form;

use DerechoBundle\Lib\Form\Type\EntityRedirectType;
use DerechoBundle\Lib\Form\Type\EntitySetType;
use DerechoBundle\Lib\Form\Type\ExpandingListType;
use DerechoBundle\Lib\Model;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Routing\Router;

class Project extends AbstractType
{
	public function buildForm(FormBuilderInterface $builder,array $options)
	{
		$builder
			->add("name","text")
			->add("description","textarea")
			->add(
				"status",
				EntityRedirectType::getClass(),
				EntityRedirectType::generateArgs(Model\Status::getClass(),$options["data"]->getStatus(),"status")
			)
			->add(
				"milestones",
				ExpandingListType::getClass(),
				ExpandingListType::generateArgs($options["data"],"project","milestone")
			)
			->add(
				"issues",
				ExpandingListType::getClass(),
				ExpandingListType::generateArgs($options["data"],"project","issue")
			)
			->add(
				"tags",
				EntitySetType::getClass(),
				EntitySetType::generateArgs(Model\Tag::getClass(),$options["data"]->getTags(),"tag")
			)
			->add("submit","submit");
	}
}
