<?php

namespace DerechoBundle\Lib\Form;

use DerechoBundle\Lib\Form\Type\ExpandingListType;
use DerechoBundle\Lib\Model;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;

class Tag extends AbstractType
{
	public function buildForm(FormBuilderInterface $builder,array $options)
	{
		$builder
			->add("name","text")
			->add("description","text")
			->add(
				"projects",
				ExpandingListType::getClass(),
				ExpandingListType::generateArgs($options["data"],"tag","project")
			)
			->add(
				"milestones",
				ExpandingListType::getClass(),
				ExpandingListType::generateArgs($options["data"],"tag","milestone")
			)
			->add(
				"issues",
				ExpandingListType::getClass(),
				ExpandingListType::generateArgs($options["data"],"tag","issue")
			)
			->add(
				"workspans",
				ExpandingListType::getClass(),
				ExpandingListType::generateArgs($options["data"],"tag","workspan")
			)
			->add("submit","submit");
	}
}
