<?php

namespace DerechoBundle\Lib\Form\Type;

use DerechoBundle\Lib\ContainerAccess;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\FormType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\Form\FormView;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;
use Symfony\Component\Routing\Router;

class EntitySetType extends EntityType
{
	private $editable;

	public function __construct()
	{
		$this->editable=self::isEditable();
		if($this->editable)
		{
			parent::__construct(ContainerAccess::get("doctrine"));
		}
	}

	/**
	 * @return bool
	 */
	private static function isEditable()
	{
		return preg_replace(
			       "/^(?:[a-zA-Z]*\\\\)*([a-zA-Z]*)Controller::.*$/",
			       "$1",
			       ContainerAccess::get("request")->attributes->get("_controller")
		       )==="Edit";
	}

	public static function generateArgs($class,$objects,$type,$required=false)
	{
		return self::isEditable()
			?["class"=>$class,"placeholder"=>"","required"=>$required,"expanded"=>true,"multiple"=>true]
			:(empty($objects)
				?[
					"objects"=>[
						[
							"disabled"=>true,
							"style"   =>"button entity standalone",
							"text"    =>"None",
							"url"     =>"",
						],
					],
				]
				:[
					"objects"=>array_map(
						function($object) use ($type)
						{
							return [
								"disabled"=>false,
								"style"   =>"button entity standalone",
								"text"    =>$object->getTitle(),
								"url"     =>ContainerAccess::get("router")->generate(
									"view",
									[
										"objectType"=>$type,
										"id"        =>$object->getId(),
									],
									Router::ABSOLUTE_URL
								),
							];
						},
						$objects
					),
				]
			);
	}

	public static function getClass()
	{
		// TODO replace with self::class, parent::class when drop 5.4
		if(!self::isEditable())
		{
			return get_called_class();
		}
		else
		{
			return 'Symfony\Bridge\Doctrine\Form\Type\EntityType';
		}
	}

	public function buildForm(FormBuilderInterface $builder,array $options)
	{
		if($this->editable)
		{
			parent::buildForm($options,$options);
		}
		else
		{
			$builder
				->setAttribute('objects',$options['objects']);
		}
	}

	public function buildView(FormView $view,FormInterface $form,array $options)
	{
		if($this->editable)
		{
			parent::buildView($view,$form,$options);
		}
		else
		{
			$view->vars["objects"]=$options["objects"];
		}
	}

	public function configureOptions(OptionsResolver $resolver)
	{
		if($this->editable)
		{
			parent::configureOptions($resolver);
		}
	}

	public function getParent()
	{
		// TODO replace with ::class when drop 5.4
		return $this->editable
			?'Symfony\Component\Form\Extension\Core\Type\ChoiceType'
			:'Symfony\Component\Form\Extension\Core\Type\FormType';
	}

	public function setDefaultOptions(OptionsResolverInterface $resolver)
	{
		if($this->editable)
		{
			parent::setDefaultOptions($resolver);
		}
		else
		{
			$resolver->setDefaults(
				[
					'objects'=>[],
				]
			);
		}
	}

	public function getBlockPrefix()
	{
		return $this->editable
			?'entity'
			:'entity_set';
	}
}
