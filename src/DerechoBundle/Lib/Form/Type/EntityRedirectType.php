<?php

namespace DerechoBundle\Lib\Form\Type;

use DerechoBundle\Lib\ContainerAccess;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\FormType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\Form\FormView;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;
use Symfony\Component\Routing\Router;

class EntityRedirectType extends EntityType
{
	private $editable;

	public function __construct()
	{
		$this->editable=self::isEditable();
		if($this->editable)
		{
			parent::__construct(ContainerAccess::get("doctrine"));
		}
	}

	/**
	 * @return bool
	 */
	public static function isEditable()
	{
		return preg_replace(
			       "/^(?:[a-zA-Z]*\\\\)*([a-zA-Z]*)Controller::.*$/",
			       "$1",
			       ContainerAccess::get("request")->attributes->get("_controller")
		       )==="Edit";
	}

	public static function generateArgs($class,$object,$type,$required=false,$restriction=null,$additional=[])
	{
		return array_merge(self::isEditable()
			?["class"=>$class,"placeholder"=>"","required"=>$required, "query_builder"=>function($er)use($restriction,$type){
				return ($restriction===null)?$restriction:
					$er->createQueryBuilder($type)
						->where($restriction);
			}]
			:[
				"disabled"=>$object===null,
				"style"   =>"button entity standalone",
				"text"    =>$object===null
					?"None"
					:$object->getTitle(),
				"url"     =>ContainerAccess::get("router")->generate(
					"view",
					[
						"objectType"=>$type,
						"id"        =>$object===null
							?0
							:$object->getId(),
					],
					Router::ABSOLUTE_URL
				),
			],
            $additional);
	}

	public static function getClass()
	{
		// TODO replace with self::class, parent::class when drop 5.4
		if(!self::isEditable())
		{
			return get_called_class();
		}
		else
		{
			return 'Symfony\Bridge\Doctrine\Form\Type\EntityType';
		}
	}

	public function buildForm(FormBuilderInterface $builder,array $options)
	{
		if($this->editable)
		{
			parent::buildForm($options,$options);
		}
		else
		{
			$builder
				->setAttribute('style',$options['style'])
				->setAttribute('text',$options['text'])
				->setAttribute('url',$options['url']);
		}
	}

	public function buildView(FormView $view,FormInterface $form,array $options)
	{
		if($this->editable)
		{
			parent::buildView($view,$form,$options);
		}
		else
		{
			$view->vars["disabled"]=$options["disabled"];
			$view->vars["url"]     =$options["url"];
			$view->vars["text"]    =$options["text"];
			$view->vars["style"]   =$options["style"];
		}
	}

	public function configureOptions(OptionsResolver $resolver)
	{
		if($this->editable)
		{
			parent::configureOptions($resolver);
		}
	}

	public function getParent()
	{
		// TODO replace with ::class when drop 5.4
		return $this->editable
			?'Symfony\Component\Form\Extension\Core\Type\ChoiceType'
			:'Symfony\Component\Form\Extension\Core\Type\FormType';
	}

	public function setDefaultOptions(OptionsResolverInterface $resolver)
	{
		if($this->editable)
		{
			parent::setDefaultOptions($resolver);
		}
		else
		{
			$resolver->setDefaults(
				[
					'style'=>'',
					'url'  =>'',
					'text' =>'Open',
				]
			);
		}
	}

	public function getBlockPrefix()
	{
		return $this->editable?'entity':'entity_redirect';
	}
}
