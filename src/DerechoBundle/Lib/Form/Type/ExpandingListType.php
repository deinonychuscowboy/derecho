<?php

namespace DerechoBundle\Lib\Form\Type;

use DerechoBundle\Lib\ContainerAccess;
use DerechoBundle\Lib\Model\Model;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\Form\FormView;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;
use Symfony\Component\Routing\Router;

/**
 * Displays an item count and [List][Add] buttons
 */
class ExpandingListType extends AbstractType
{
	public static function generateArgs($object,$parentType,$type)
	{
		$className=Model::name($type);
		$fetchName="get".$className."s";
		$itemCount=count(
			$object::filterActive($object->$fetchName())
		);

		return [
			"disabled"=>!self::isEditable(),
			"style"   =>"button",
			"textview"=>"List",
			"textadd" =>"Add",
			"status"  =>$itemCount." items",
			"urlview" =>ContainerAccess::get("router")->generate(
				"list_in",
				[
					"objectType"=>$type,
					"parentType"=>$parentType,
					"parentId"  =>$object->getId()
						?:0,
				],
				Router::ABSOLUTE_URL
			),
			"urladd"  =>ContainerAccess::get("router")->generate(
				"create_in",
				[
					"objectType"=>$type,
					"parentType"=>$parentType,
					"parentId"  =>$object->getId()
						?:0,
				],
				Router::ABSOLUTE_URL
			),
		];
	}

	/**
	 * @return bool
	 */
	private static function isEditable()
	{
		return preg_replace(
			       "/^(?:[a-zA-Z]*\\\\)*([a-zA-Z]*)Controller::.*$/",
			       "$1",
			       ContainerAccess::get("request")->attributes->get("_controller")
		       )==="Edit";
	}

	/**
	 * Get the class name.
	 *
	 * TODO Can be replaced by ::class when we drop 5.4 support.
	 *
	 * @return string
	 */
	public static function getClass()
	{
		return get_called_class();
	}

	public function buildForm(FormBuilderInterface $builder,array $options)
	{
		$builder
			->setAttribute('urlview',$options['urlview'])
			->setAttribute('urladd',$options['urladd'])
			->setAttribute('style',$options['style'])
			->setAttribute('status',$options['status'])
			->setAttribute('textview',$options['textview'])
			->setAttribute('textadd',$options['textadd']);
	}

	public function buildView(FormView $view,FormInterface $form,array $options)
	{
		$view->vars["disabled"]=$options["disabled"];
		$view->vars["urlview"] =$options["urlview"];
		$view->vars["urladd"]  =$options["urladd"];
		$view->vars["textview"]=$options["textview"];
		$view->vars["textadd"] =$options["textadd"];
		$view->vars["style"]   =$options["style"];
		$view->vars["status"]=$options["status"];
	}

	public function setDefaultOptions(OptionsResolverInterface $resolver)
	{
		$resolver->setDefaults(
			[
				'style'   =>'',
				'status'=>'',
				'urlview' =>'',
				'urladd'  =>'',
				'textview'=>'Open',
				'textadd' =>'Create',
			]
		);
	}
}
