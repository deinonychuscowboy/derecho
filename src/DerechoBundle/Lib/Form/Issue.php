<?php

namespace DerechoBundle\Lib\Form;

use DerechoBundle\Lib\Form\Type\EntityRedirectType;
use DerechoBundle\Lib\Form\Type\EntitySetType;
use DerechoBundle\Lib\Form\Type\ExpandingListType;
use DerechoBundle\Lib\Model;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;

class Issue extends AbstractType
{
	public function buildForm(FormBuilderInterface $builder,array $options)
	{
		$project_id=$options["data"]->getProject()!==null?$options["data"]->getProject()->getId():null;
		$builder
			->add(
				"project",
				EntityRedirectType::getClass(),
				EntityRedirectType::generateArgs(Model\Project::getClass(),$options["data"]->getProject(),"project",true,null,$project_id===null||!EntityRedirectType::isEditable()?[]:["disabled"=>true])
			)
			->add("summary","text")
			->add("description","textarea")
			->add("openedTimestamp","date",["input"=>"timestamp","label"=>"Opened","widget"=>"single_text"])
			->add("closedTimestamp","date",["input"=>"timestamp","label"=>"Closed","placeholder"=>"","required"=>false,"widget"=>"single_text"])
			->add(
				"status",
				EntityRedirectType::getClass(),
				EntityRedirectType::generateArgs(Model\Status::getClass(),$options["data"]->getStatus(),"status",true)
			);
		if($project_id!==null)
		{
			// only add if has a project
			$builder->add(
				"milestone",
				EntityRedirectType::getClass(),
				EntityRedirectType::generateArgs(
					Model\Milestone::getClass(),
					$options["data"]->getMilestone(),
					"milestone",
					false,
					$project_id!==null?"milestone.project=".$project_id:null
				)
			);
		}
			$builder->add(
				"workspans",
				ExpandingListType::getClass(),
				ExpandingListType::generateArgs($options["data"],"issue","workspan")
			)
			->add(
				"tags",
				EntitySetType::getClass(),
				EntitySetType::generateArgs(Model\Tag::getClass(),$options["data"]->getTags(),"tag")
			)
			->add("submit","submit");
	}
}
