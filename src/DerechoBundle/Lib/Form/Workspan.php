<?php

namespace DerechoBundle\Lib\Form;

use DerechoBundle\Lib\Form\Type\EntityRedirectType;
use DerechoBundle\Lib\Form\Type\EntitySetType;
use DerechoBundle\Lib\Model;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;

class Workspan extends AbstractType
{
	public function buildForm(FormBuilderInterface $builder,array $options)
	{
		$builder// TODO this doesn't seem right, should it be a list?
		->add(
			"issue",
			EntityRedirectType::getClass(),
			EntityRedirectType::generateArgs(Model\Issue::getClass(),$options["data"]->getIssue(),"issue",true)
		)
		->add("startTimestamp","date",["input"=>"timestamp","label"=>"Start","data"=>time()])
		->add("endTimestamp","date",["input"=>"timestamp","label"=>"End","required"=>false])
		->add("description","text")
		->add(
			"tags",
			EntitySetType::getClass(),
			EntitySetType::generateArgs(Model\Tag::getClass(),$options["data"]->getTags(),"tag")
		)
		->add("submit","submit");
	}
}
