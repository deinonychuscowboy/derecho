<?php

namespace DerechoBundle\Lib\Form\Settings;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;

class Scheme extends AbstractType
{
	public function buildForm(FormBuilderInterface $builder,array $options)
	{
		$builder->add(
			"value",
			"choice",
			[
				"choices"=>[
					"scheme-mud"         =>"Mud",
					"scheme-clay"        =>"Clay",
					"scheme-sand"        =>"Sand",
					"scheme-weed"        =>"Weed",
					"scheme-derecho"     =>"Derecho",
					"scheme-sky"         =>"Sky",
					"scheme-mist"        =>"Mist",
					"scheme-nightshade"  =>"Nightshade",
					"scheme-amaranth"    =>"Amaranth",
					"scheme-cumulonimbus"=>"Cumulonimbus",
					"scheme-stratus"     =>"Stratus",
					"scheme-cumulus"     =>"Cumulus",
					"scheme-cirrus"      =>"Cirrus",
				],
				"label"  =>false,
			]
		);
	}
}
