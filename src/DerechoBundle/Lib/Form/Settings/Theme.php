<?php

namespace DerechoBundle\Lib\Form\Settings;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;

class Theme extends AbstractType
{
	public function buildForm(FormBuilderInterface $builder,array $options)
	{
		$builder->add("value","choice",["choices"=>["theme-clean"=>"Clean","theme-rich"=>"Rich","theme-plateau"=>"Plateau"],"label"=>false]);
	}
}
