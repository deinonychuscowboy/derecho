<?php

namespace DerechoBundle\Lib\Form;

use DerechoBundle\Lib\Form\Type\EntityRedirectType;
use DerechoBundle\Lib\Form\Type\EntitySetType;
use DerechoBundle\Lib\Form\Type\ExpandingListType;
use DerechoBundle\Lib\Model;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;

class Milestone extends AbstractType
{
	public function buildForm(FormBuilderInterface $builder,array $options)
	{
		$builder
			->add(
				"project",
				EntityRedirectType::getClass(),
				EntityRedirectType::generateArgs(Model\Project::getClass(),$options["data"]->getProject(),"project",true)
			)
			->add("version","text")
			->add(
				"status",
				EntityRedirectType::getClass(),
				EntityRedirectType::generateArgs(Model\Status::getClass(),$options["data"]->getStatus(),"status")
			)
			->add(
				"issues",
				ExpandingListType::getClass(),
				ExpandingListType::generateArgs($options["data"],"milestone","issue")
			)
			->add(
				"tags",
				EntitySetType::getClass(),
				EntitySetType::generateArgs(Model\Tag::getClass(),$options["data"]->getTags(),"tag")
			)
			->add("submit","submit");
	}
}
