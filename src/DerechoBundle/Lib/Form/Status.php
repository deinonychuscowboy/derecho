<?php

namespace DerechoBundle\Lib\Form;

use DerechoBundle\Lib\Form\Type\ExpandingListType;
use DerechoBundle\Lib\Model;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;

class Status extends AbstractType
{
	public function buildForm(FormBuilderInterface $builder,array $options)
	{
		$builder
			->add("name","text")
			->add("description","text")
			->add("archival","checkbox",["required"=>false])
			->add(
				"projects",
				ExpandingListType::getClass(),
				ExpandingListType::generateArgs($options["data"],"status","project")
			)
			->add(
				"milestones",
				ExpandingListType::getClass(),
				ExpandingListType::generateArgs($options["data"],"status","milestone")
			)
			->add(
				"issues",
				ExpandingListType::getClass(),
				ExpandingListType::generateArgs($options["data"],"status","issue")
			)
			->add("submit","submit");
	}
}
