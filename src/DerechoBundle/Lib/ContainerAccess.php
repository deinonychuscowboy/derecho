<?php

namespace DerechoBundle\Lib;

use Symfony\Component\DependencyInjection\ContainerInterface;

class ContainerAccess
{
	/**
	 * @var ContainerInterface
	 */
	private static $container;

	public static function get($key)
	{
		return self::$container->get($key);
	}

	public static function set(ContainerInterface $container)
	{
		self::$container=$container;
	}
}
