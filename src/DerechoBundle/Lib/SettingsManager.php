<?php

namespace DerechoBundle\Lib;

use DerechoBundle\Lib\Model\Setting;
use DomainException;

class SettingsManager
{
	private static $settings_keys=[
		"title"=>"Derecho",
		"theme"=>"theme-clean",
		"scheme"=>"scheme-derecho",
	];

	/**
	 * @param $key
	 *
	 * @return mixed
	 */
	public static function get($key)
	{
		if(!array_key_exists($key,self::$settings_keys))
		{
			throw new DomainException("Invalid setting.");
		}
		$setting=ContainerAccess::get("doctrine")->getRepository(Setting::getClass())->findBy(["id"=>$key]);
		if(!empty($setting))
		{
			return $setting[0];
		}
		else
		{
			return new Setting($key,self::$settings_keys[$key]);
		}
	}

	/**
	 * @return array
	 */
	public static function getAll()
	{
		$settings=[];

		foreach(self::$settings_keys as $key=>$default)
		{
			$settings[$key]=self::get($key);
		}

		return $settings;
	}
}
