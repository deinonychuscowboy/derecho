<?php

namespace DerechoBundle\Lib;

use DerechoBundle\Lib\Model\Model;
use Doctrine\Bundle\DoctrineBundle\Registry;
use Symfony\Component\Form\FormFactory;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Router;

class FormHandler
{
	/**
	 * @var Registry
	 */
	private $doctrine;
	/**
	 * @var FormFactory
	 */
	private $formFactory;
	/**
	 * @var Router
	 */
	private $router;

	/**
	 * FormHandler constructor.
	 *
	 * @param Registry    $doctrine
	 * @param Router      $router
	 * @param FormFactory $formFactory
	 */
	public function __construct(Registry $doctrine,Router $router,FormFactory $formFactory)
	{
		$this->doctrine   =$doctrine;
		$this->router     =$router;
		$this->formFactory=$formFactory;
	}

	/**
	 * @param \Symfony\Component\HttpFoundation\Request $request
	 * @param                                           $objectType
	 * @param                                           $id
	 * @param                                           $editable
	 * @param array                                     $data
	 *
	 * @return array|\Symfony\Component\HttpFoundation\Response
	 */
	public function createObjectForm(Request $request,$objectType,$id,$editable,$data=[])
	{
		/**
		 * @var Model $obj
		 */
		$obj                 =null;
		$objectClass         =Model::name($objectType);
		$objectClassQualified="DerechoBundle\\Lib\\Model\\".$objectClass;
		$isNew               =$id===null;
		if($isNew)
		{
			$obj=new $objectClassQualified();
		}
		else
		{
			$obj=$objectClassQualified::load($id);
		}

		foreach($data as $field=>$datum)
		{
			$dataSetter="set".Model::name($field);
			$obj->$dataSetter($datum);
		}

		$formName="DerechoBundle\\Lib\\Form\\".$objectClass;
		$form    =$this->formFactory->create(
			$formName,
			$obj,
			$editable
				?[]
				:["disabled"=>true]
		);

		$form->handleRequest($request);

		if($form->isSubmitted()&&$form->isValid())
		{
			$this->doctrine->getManager()->persist($obj);
			$this->doctrine->getManager()->flush();

			return new RedirectResponse(
				$this->router->generate("view",["objectType"=>$objectType,"id"=>$obj->getId()],Router::ABSOLUTE_URL)
			);
		}

		return [
			"form"      =>$form->createView(),
			"isNew"     =>$isNew,
			"objectType"=>strtolower($objectType),
			"object"    =>$obj,
		    "parentType"=>strtolower($obj->getParentType()),
		    "parentId"=>$obj->getParentId(),
		];
	}

	/**
	 * @param \Symfony\Component\HttpFoundation\Request $request
	 * @param                                           $settings
	 *
	 * @return array|Response
	 *
	 */
	public function createSettingsForm(Request $request,$settings)
	{
		$base_form=$this->formFactory->create();
		foreach($settings as $key=>$setting)
		{
			$class   =Model::name($key);
			$formName="DerechoBundle\\Lib\\Form\\Settings\\".$class;

			$form=$this->formFactory->create(
				$formName,
				$setting,
				["auto_initialize"=>false]
			);
			$base_form->add($form);
		}

		$base_form->add("submit","submit");

		$base_form->handleRequest($request);

		if($base_form->isSubmitted()&&$base_form->isValid())
		{
			foreach($settings as $setting)
			{
				$this->doctrine->getManager()->persist($setting);
			}
			$this->doctrine->getManager()->flush();
		}

		return [
			"form"=>$base_form->createView(),
		];
	}
}
