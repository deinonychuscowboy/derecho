<?php

namespace DerechoBundle\Controller;

use DerechoBundle\Lib\ContainerAccess;
use DerechoBundle\Lib\Model\Model;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\Request;

/**
 * @Route("/")
 */
class ViewController extends Controller
{
	public function setContainer(ContainerInterface $container=null)
	{
		parent::setContainer($container);
		ContainerAccess::set($this->container);
	}

	/**
	 * @Route("/list/{objectType}", name="list")
     * @Route("/list/{objectType}/", name="list_symfonyisdumb")
	 * @Route("/list/all/{objectType}", name="list_all")
     * @Route("/list/all/{objectType}/", name="list_all_symfonyisdumb")
	 * @Template()
	 */
	public function listAction($objectType)
	{
		$all=strpos($this->get("request")->get("_route"),"list_all")!==false;
		$objectClass="DerechoBundle\\Lib\\Model\\".Model::name($objectType);
		if($all)
		{
			$objects=$objectClass::load();
		}
		else
		{
			$objects=$objectClass::loadActive();
		}

		return [
			"all"=>$all,
			"objects"   =>$objects,
			"objectType"=>strtolower($objectType),
		];
	}

	/**
	 * TODO nicer dashboard
	 *
	 * @Route("/", name="home")
	 * @Template("DerechoBundle:View:list.html.twig")
	 */
	public function defaultAction(){
		return $this->listAction("project");
	}

	/**
	 * @Route("/list/{objectType}/in/{parentType}/{parentId}", name="list_in")
     * @Route("/list/{objectType}/in/{parentType}/{parentId}/", name="list_in_symfonyisdumb")
	 * @Route("/list/all/{objectType}/in/{parentType}/{parentId}", name="list_all_in")
     * @Route("/list/all/{objectType}/in/{parentType}/{parentId}/", name="list_all_in_symfonyisdumb")
	 * @Template()
	 */
	public function listInAction($objectType,$parentType,$parentId)
	{
		$all=strpos($this->get("request")->get("_route"),"list_all")!==false;
		$objectClass         =Model::name($objectType);
		$parentClass         =Model::name($parentType);
		$parentClassQualified="DerechoBundle\\Lib\\Model\\".$parentClass;
		$parent              =$parentClassQualified::load($parentId);
		$fetchMethod         ="get".$objectClass."s";
		$objects             =$parent->$fetchMethod();
		if(!$all){
			$objects=$parent::filterActive($objects);
		}

		return [
			"all"=>$all,
			"objects"   =>$objects,
			"objectType"=>strtolower($objectType),
			"parentType"=>strtolower($parentType),
			"parent"    =>$parent,
		];
	}

	/**
	 * @Route("/view/{objectType}/{id}", name="view")
     * @Route("/view/{objectType}/{id}/", name="view_symfonyisdumb")
	 * @Template()
	 */
	public function viewAction(Request $request,$objectType,$id)
	{
		return $this->get("derecho.formHandler")->createObjectForm($request,$objectType,$id,false);
	}
}
