<?php

namespace DerechoBundle\Controller;

use DerechoBundle\Lib\ContainerAccess;
use DerechoBundle\Lib\Model\Model;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\Request;

/**
 * @Route("/edit")
 */
class EditController extends Controller
{
	public function setContainer(ContainerInterface $container=null)
	{
		parent::setContainer($container);
		ContainerAccess::set($this->container);
	}

	/**
	 * @Route("/{objectType}/{id}", defaults={"id"=null}, name="edit")
	 * @Route("/{objectType}/{id}/", defaults={"id"=null}, name="edit_symfonyisdumb")
	 * @Route("/{objectType}", defaults={"id"=null}, name="create")
	 * @Route("/{objectType}/", defaults={"id"=null}, name="create_symfonyisdumb")
	 * @Route("/{objectType}/{parentType}/{parentId}", defaults={"id"=null}, name="create_in")
	 * @Route("/{objectType}/{parentType}/{parentId}/", defaults={"id"=null}, name="create_in_symfonyisdumb")
	 * @Template()
	 */
	public function editAction(Request $request,$objectType=null,$id=null,$parentType=null,$parentId=null)
	{
		$data=[];
		if($parentId!==null&&$parentType!==null)
		{
			$parentClass      ="DerechoBundle\\Lib\\Model\\".Model::name($parentType);
			$data[$parentType]=$parentClass::load($parentId);
		}
		$response=$this->get("derecho.formHandler")->createObjectForm($request,$objectType,$id,true,$data);

		return $response;
	}
}
