<?php

namespace DerechoBundle\Controller;

use DerechoBundle\Lib\ContainerAccess;
use DerechoBundle\Lib\Model\Model;
use DerechoBundle\Lib\Model\Setting;
use DerechoBundle\Lib\SettingsManager;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\Request;

class SettingsController extends Controller
{

	public function setContainer(ContainerInterface $container=null)
	{
		parent::setContainer($container);
		ContainerAccess::set($this->container);
	}

	/**
	 * @Route("/settings", name="settings")
	 * @Route("/settings/", name="settings_symfonyisdumb")
	 * @Template()
	 */
	public function settingsAction(Request $request)
	{
		$settings=SettingsManager::getAll();
		$response=$this->get("derecho.formHandler")->createSettingsForm($request,$settings);

		return $response;
	}
}
