#!/bin/bash

composer install
rm -rf app/cache/*
app/console assets:install
app/console assets:install --env=prod
app/console doctrine:schema:update --force
app/console assetic:dump
app/console assetic:dump --env=prod

chown -R www-data app/cache
chown -R www-data app/logs
